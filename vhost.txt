<VirtualHost *:88>
    ServerName prdlog.it.tyras
    DocumentRoot "${INSTALL_DIR}/www/prdlog/public"
    <Directory "${INSTALL_DIR}/www/prdlog/public">
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>